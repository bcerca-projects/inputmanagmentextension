FROM node:16.20.1-alpine3.17

WORKDIR /app

RUN apk add --update --no-cache \
    make \
    g++ \
    jpeg-dev \
    cairo-dev \
    giflib-dev \
    pango-dev \
    libtool \
    autoconf \
    automake

COPY ./ /app

RUN npm ci

# CMD [ "npm","run","dev" ]

ENTRYPOINT ["tail", "-f", "/dev/null"]