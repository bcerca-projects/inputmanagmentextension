import { DispayEnum } from '@state/PopUp';
import './app.scss';
import App from './App.svelte';
import * as state from '@state'
import { AvaibleTracksReplacer } from '@logic-modules/AvaibleTracksReplacer';
import { confirmTracks$, selectedVideo$, selectedAudio$ } from '@state/Media';
import { lastValueFrom, take } from 'rxjs';



function openInputManager() {
  const InputManagerAppWrapper = document.getElementById('InputManager-app') ?? (() => {
    const newNode = document.createElement("div");
    document.body.append(newNode);
    return newNode;
  })()
  if (!window?.["InputManager"]) {
  window["InputManager"] = {
    app: new App({
      target: InputManagerAppWrapper,
    }),
    state,
  };}
  state.PopUp.display$.next(DispayEnum.flex);
}

function closeInputManager() {
  state.PopUp.display$.next(DispayEnum.none);
}

window['openInputManager'] = openInputManager;


AvaibleTracksReplacer.setTrunk(async () => {
  openInputManager();
  await lastValueFrom(confirmTracks$.pipe(take(1)))
}).setFilter(function(track) { return [selectedVideo$.getValue()?.label, selectedAudio$.getValue()?.label].includes(track.label) })

export default {
  openInputManager,
  closeInputManager
};
