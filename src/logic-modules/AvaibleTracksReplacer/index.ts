type TrackFilter = (info?: { label: string }) => (Promise<boolean> | boolean)
type DeviceFilter = (info?: { label: string }) => (Promise<boolean> | boolean)
export class AvaibleTracksReplacer {
    private _trackFilter: TrackFilter = Boolean
    private _deviceFilter: DeviceFilter = Boolean
    private _originalGetUserMedia: (constraints?: MediaStreamConstraints) => Promise<MediaStream>;
    private _originalEnumerateDevices: () => Promise<MediaDeviceInfo[]>
    private _trunkUntil: () => Promise<void> | void = () => { };
    private constructor() {
        this._originalGetUserMedia = navigator.mediaDevices.getUserMedia.bind(navigator.mediaDevices)
        this._originalEnumerateDevices = navigator.mediaDevices.enumerateDevices.bind(navigator.mediaDevices)
        const fakeGetUserMedia: () => Promise<MediaStream> = async (constraints?: MediaStreamConstraints) => {
            return this._originalGetUserMedia(constraints).then(async value => {
                const fakeMediaStream = new MediaStream()
                const tracks = value.getTracks();
                await this._trunkUntil();
                for (const track of tracks) {
                    if (await this._trackFilter(track)) {
                        fakeMediaStream.addTrack(track);
                    }
                }
                return fakeMediaStream;
            })
        }
        const fakeEnumerateDevices: () => Promise<MediaDeviceInfo[]> = async () => {
            const devicesInfo = await this._originalEnumerateDevices();
            const fakeInfo: MediaDeviceInfo[] = []
            await this._trunkUntil();
            for (const deviceInfo of devicesInfo) {
                if (await this._deviceFilter(deviceInfo)) {
                    fakeInfo.push(deviceInfo)
                }
            }
            return fakeInfo;
        }

        navigator.mediaDevices.getUserMedia = fakeGetUserMedia;
        navigator.mediaDevices.enumerateDevices = fakeEnumerateDevices;
    }
    private static _instance: AvaibleTracksReplacer;

    /**
     * Запускает замену треков по фильтру
     * @returns
     */
    static run(): AvaibleTracksReplacer {
        this._instance = this._instance ?? new AvaibleTracksReplacer();
        return this._instance
    }

    /**
     * Устанавливает фильтр для треков.
     * Если сущность не была запущена, то запускает ее с новым фильтром
     * @param filter
     * @returns
     */
    static setFilter(filter: TrackFilter | DeviceFilter) {
        (this.run())._trackFilter = filter;
        (this.run())._deviceFilter = filter;
        return AvaibleTracksReplacer;
    }

    /**
     * Нужно чтобы установить функцию которая вызовится перед тем как отдать список треков внешней странице
     */
    public static setTrunk(value: () => Promise<void>) {
        (this.run())._trunkUntil = value;
        return AvaibleTracksReplacer
    }

    /**
     * Берет оригинальные аудиодорожки без фильтрации
     *  Если сущность не была запущена, то запускает ее
     * @returns
     */
    static getOriginalTracks(constraints?: MediaStreamConstraints) {
        return this.run()._originalGetUserMedia(constraints)
    }

    /**
     * Берет оригинальныей список устройств
     *  Если сущность не была запущена, то запускает ее
     * @returns
     */
    static getEnumerateDevices() {
        return this.run()._originalEnumerateDevices()
    }

    static destroy() {
        if (!this._instance) {
            return;
        }
        navigator.mediaDevices.getUserMedia = this._instance._originalGetUserMedia;
    }
}