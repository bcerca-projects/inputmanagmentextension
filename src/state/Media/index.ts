import { BehaviorSubject, Subject, merge } from "rxjs";
import { writable } from "svelte/store";
import { AvaibleTracksReplacer} from "@logic-modules/AvaibleTracksReplacer"
export const videos$ = writable<MediaDeviceInfo[]>([])
export const audios$ = writable<MediaDeviceInfo[]>([])

export const refreshDevicesList$ = new Subject<void>();

export const selectedVideo$ = new BehaviorSubject<MediaDeviceInfo | undefined>(null);
export const selectedAudio$ = new BehaviorSubject<MediaDeviceInfo | undefined>(null);

export const confirmTracks$ = new Subject<void>();

refreshDevicesList$.subscribe(async () => {
    const info = await AvaibleTracksReplacer.getEnumerateDevices();
    videos$.set(info.filter(v => v.kind === "videoinput"));
    audios$.set(info.filter(v => v.kind === "audioinput"));
})
