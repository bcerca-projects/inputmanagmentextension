import { BehaviorSubject, Subject } from "rxjs";

export const currentTab$ = new BehaviorSubject<chrome.tabs.Tab | undefined>(undefined)

export const refreshTabs$ = new Subject<void>();

refreshTabs$.subscribe(() => {
    chrome.tabs.query({ active: true }, (tabs) => {
        const tab = tabs[0];
        currentTab$.next(tab);
    })
})

