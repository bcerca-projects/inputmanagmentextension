import { BehaviorSubject } from "rxjs";

export const debug$ = new BehaviorSubject<boolean>(true);