import { BehaviorSubject } from "rxjs";

export enum DispayEnum {
    initial = "initial",
    flex = "flex",
    none = "none"
}

export const display$ = new BehaviorSubject<DispayEnum>(DispayEnum.flex)