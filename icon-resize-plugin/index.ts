import * as path from "path";
import * as fs from "fs";
import { loadImage , createCanvas} from "canvas"

interface Props {
    iconPath: string, outputFolder: string, sizes:{ width: number, height: number}[]
}
export const IconResizePlugins = function({ iconPath, outputFolder, sizes }: Props) {
    return {
        name: 'icon-resize-plugin',
        async buildStart(...args) {
            const originalFilePath = iconPath;
            const ext = path.extname(originalFilePath);
            const fileName = path.basename(originalFilePath, ext);
            // const image = gm(originalFilePath).setFormat(ext);
          await Promise.all(sizes.map(async (size) => {
            const outputFilePath = path.resolve(outputFolder, `./${fileName}-${size.width}x${size.height}${ext}`);
             await resizeImage(originalFilePath,outputFilePath,size.width,size.width)
            }));
            console.log(" Icons resized ")
        }
    };
};

async function resizeImage(sourcePath: string, targetPath:string, targetWidth: number, targetHeight: number) {
  const image = await loadImage(sourcePath);
  const canvas = createCanvas(targetWidth, targetHeight);
  const context = canvas.getContext('2d');

  // set background color
  context.fillStyle = '#ffffff';
  context.fillRect(0, 0, canvas.width, canvas.height);

  // calculate image dimensions and aspect ratio
  const aspectRatio = image.width / image.height;
  const maxDimension = Math.max(targetWidth, targetHeight);
  const targetAspectRatio = targetWidth / targetHeight;

  let drawWidth = maxDimension;
  let drawHeight = maxDimension / aspectRatio;

  if (aspectRatio < targetAspectRatio) {
    drawWidth = drawHeight * targetAspectRatio;
    drawHeight = drawHeight;
  } else {
    drawWidth = drawWidth;
    drawHeight = drawWidth / targetAspectRatio;
  }

  // draw the resized image on the canvas
  const drawX = (canvas.width - drawWidth) / 2;
  const drawY = (canvas.height - drawHeight) / 2;
  context.drawImage(image, drawX, drawY, drawWidth, drawHeight);

  // output the resized image
  const buffer = canvas.toBuffer('image/jpeg');
  await fs.promises.writeFile(targetPath, buffer);
}