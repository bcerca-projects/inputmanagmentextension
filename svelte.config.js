import { vitePreprocess } from '@sveltejs/vite-plugin-svelte'
/** @type {import('@sveltejs/kit').Config} */
export default {
  // Consult https://svelte.dev/docs#compile-time-svelte-preprocess
  // for more information about preprocessors
  preprocess: vitePreprocess(),
  kit: {
    alias: {
      '@common': './src/lib/common',
      '@lib': './src/lib',
      '@directives': './src/custom-directives',
      '@assets': './src/assets/',
      '@logic-modules': './src/logic-modules/'
    }
  }
}
