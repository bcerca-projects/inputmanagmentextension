import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import { IconResizePlugins } from './icon-resize-plugin/index'
import * as path from "path";

// https://vitejs.dev/config/
export default defineConfig(({ command, mode, ssrBuild }) => ({
  build: {
    lib: {
      entry: [ path.resolve(__dirname, './src/entrypoints/PageScript/content_main.ts'), path.resolve(__dirname, './src/entrypoints/PopUpScript/popup.ts')],
      name: "content",
      formats: ['es'],
      fileName: (format, name) => `${name}.js`,
    }

  },
  resolve: {
    alias: {
      "@common": path.resolve(__dirname, './src/lib/common'),
      "@lib": path.resolve(__dirname, './src/lib'),
      "@directives": path.resolve(__dirname, './src/custom-directives'),
      "@state": path.resolve(__dirname, './src/state'),
      '@assets': path.resolve(__dirname, './src/assets/'),
      '@logic-modules': path.resolve(__dirname, './src/logic-modules/'),
    }
  },
  plugins: [svelte(),
  command === "build" && IconResizePlugins({
    iconPath: path.resolve(__dirname, './src/assets/icon.png'),
    outputFolder: path.resolve(__dirname, './icons'),
    sizes: [{
      width: 16,
      height: 16
    }, {
      width: 32,
      height: 32
    }, {
      width: 48,
      height: 48
    }, {
      width: 128,
      height: 128
    }]
  })].filter(Boolean),
  server: {
    port: 5001,
    host: true
  }
}));
